from django.urls import path

from .views import product_list


app_name = 'main'

urlpatterns = [
    path('', product_list, name='list'),
]
