from django.db import models


class Product(models.Model):
    title = models.CharField(max_length=150)
    cost = models.DecimalField(
        max_digits=8, 
        decimal_places=2,
        default=0,
    )

    def __str__(self):
        return self.title
