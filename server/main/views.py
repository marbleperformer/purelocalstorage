from functools import reduce
from django.db.models import Q
from django.shortcuts import (
    get_list_or_404, get_object_or_404, render
)
from django.http import JsonResponse

from .models import Product


def product_json_list(request):
    query_params = (
        (key, list(map(int, value.split(','))) if key.endswith('_in') else value)
        for key, value in request.GET.items()
    )
    query = get_list_or_404(
        Product,
        reduce(
            lambda store, itm: store | Q(**{itm[0]: itm[1]}),
            query_params,
            Q()
        )
    )

    return JsonResponse(
        list(
            map(
                lambda itm: {
                    'id': itm.id,
                    'title': itm.title
                },
                query
            )
        ),
        safe=False
    )


def product_list(request):
    query = get_list_or_404(Product)

    return render(
        request, 
        'main/list.html', 
        {'object_list': query}
    )
